const backend = 'http://localhost:3000'
describe('Booking API Tests', () => {
	it('should create a new booking', () => {
		const tripId = '6622377a4c6d311425bdb120' // Replace with a valid tripId

		cy.request('POST', `${backend}/cart/`, { tripId: tripId }).then((response) => {
			expect(response.status).to.equal(200)
			expect(response.body.result).to.be.true
		})
	})

	it('should get all unpaid bookings', () => {
		cy.request('GET', `${backend}/cart/`).then((response) => {
			expect(response.status).to.equal(200)
			expect(response.body.result).to.be.true
			expect(response.body.bookings).to.exist
		})
	})
})

describe('Booking API Tests', () => {
	it('should create a new booking', () => {
		const tripId = '6622377a4c6d311425bdb120' // Replace with a valid tripId

		cy.request('POST', `${backend}/cart/`, { tripId: tripId }).then((response) => {
			expect(response.status).to.equal(200)
			expect(response.body.result).to.be.true
		})
	})

	it('should get all unpaid bookings', () => {
		cy.request('GET', `${backend}/cart/`).then((response) => {
			expect(response.status).to.equal(200)
			expect(response.body.result).to.be.true
			expect(response.body.bookings).to.exist
		})
	})

	it('should delete a booking by tripId', () => {
		const tripIdToDelete = '6622377a4c6d311425bdb120' // Replace with a valid tripId

		cy.request('DELETE', `${backend}/cart/${tripIdToDelete}`).then((response) => {
			expect(response.status).to.equal(200)
			expect(response.body.result).to.be.true
			expect(response.body.bookings).to.exist
		})
	})
})
