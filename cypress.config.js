module.exports = {
	e2e: {
		baseUrl: process.env.BACKEND_URL,
		specPattern: 'cypress/integration/*.spec.{js,jsx,ts,tsx}',
		setupNodeEvents(on, config) {
			// implement node event listeners here
		},
	},
	env: {
		backendUrl: `${process.env.BACKEND_URL}`
	}
}